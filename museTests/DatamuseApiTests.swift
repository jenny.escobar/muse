//
//  museTests.swift
//  museTests
//
//  Created by Jenny escobar on 13/10/21.
//

import XCTest
@testable import muse

class DatamuseAPITests: XCTestCase {
    var session: URLSessionMock!
    var manager: NetworkManager!
    var datamuseClient: DatamuseClient!
    
    override func setUp() {
        session = URLSessionMock()
        manager = NetworkManager(session: session)
        datamuseClient = DatamuseClient(networkManager: manager)
    }
    
    
    func testGetSynonymsInEnglish(){
        let jsonData = loadLocalfile(name: "EnglishSynonyms", extension: "json")
        session.data = jsonData
        var synonymsResults: [Synonym]?
        let synonymsExpectation = expectation(description: "synonims")
        _ = datamuseClient.getMuseByMeaning(word: "sound"){ (synonyms, error) in
            synonymsResults = synonyms
            synonymsExpectation.fulfill()
          }
        self.waitForExpectations(timeout: 4){ error in
            XCTAssertNotNil(synonymsResults)
            XCTAssertEqual(synonymsResults?[0].word, "audio")
        }
    }
    
    func testGetSynonymsInSpanish(){
        let jsonData = loadLocalfile(name: "SpanishSynonyms", extension: "json")
        session.data = jsonData
        var synonymsResults: [Synonym]?
        let synonymsExpectation = expectation(description: "synonims")
        _ = datamuseClient.getMuseByMeaning(word: "sonido"){ (synonyms, error) in
            synonymsResults = synonyms
            synonymsExpectation.fulfill()
          }
        self.waitForExpectations(timeout: 4){ error in
            XCTAssertNotNil(synonymsResults)
            XCTAssertEqual(synonymsResults?[0].word, "sonido")
        }
    }
}
