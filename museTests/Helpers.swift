//
//  Helpers.swift
//  museTests
//
//  Created by Jenny escobar on 13/10/21.
//

import XCTest

extension XCTestCase {
    func loadLocalfile(name: String, extension: String) -> Data {
        let bundle = Bundle(for: type(of: self))
        let url = bundle.url(forResource: name, withExtension: `extension` )
        return try! Data(contentsOf: url!)
    }
}
