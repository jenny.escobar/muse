//
//  DatamuseClient.swift
//  muse
//
//  Created by Jenny escobar on 13/10/21.
//

import Foundation

class DatamuseClient {
    private let networkManager: NetworkManager
    init(networkManager: NetworkManager = .init()) {
        self.networkManager = networkManager
    }

    enum DatamuseEndpoints {
        static let base = "https://api.datamuse.com/words?"
        
        case getMuseByMeaning(String)
        case getMuseByMeaningInSpanish(String)
        
        var stringValue: String {
            switch self {
            case .getMuseByMeaning(let word):return DatamuseEndpoints.base+"ml=\(word.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")"
            case .getMuseByMeaningInSpanish(let word):return DatamuseEndpoints.base+"ml=\(word.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")&v=es"
            }
        }
        
        var url: URL{
            return URL(string: stringValue)!
        }
    }
    
    func getMuseByMeaning(word: String, completion: @escaping ([Synonym], Error?)-> Void) -> URLSessionDataTask{
        let task = networkManager.taskForGetRequest(url: DatamuseEndpoints.getMuseByMeaning(word).url, responseType: [Synonym].self) { response, error in
            if let response = response {
                completion(response, nil)
            } else {
                completion([], error)
            }
        }
        return task
    }
    
    func getMuseByMeaningInSpanish(word: String, completion: @escaping ([Synonym], Error?)-> Void) -> URLSessionDataTask{
        let task = networkManager.taskForGetRequest(url: DatamuseEndpoints.getMuseByMeaningInSpanish(word).url, responseType: [Synonym].self) { response, error in
            if let response = response {
                completion(response, nil)
            } else {
                completion([], error)
            }
        }
        return task
    }
}
