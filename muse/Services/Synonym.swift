//
//  Synonym.swift
//  muse
//
//  Created by Jenny escobar on 13/10/21.
//

import Foundation

class Synonym: Codable{
    let word: String
    let score: Int
    let tags: [String]?
}
