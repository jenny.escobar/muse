//
//  ErrorResponse.swift
//  muse
//
//  Created by Jenny escobar on 13/10/21.
//

import Foundation

struct ErrorResponse: Codable {
    let statusCode: Int
    let statusMessage: String
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "code"
        case statusMessage = "message"
    }
}

extension ErrorResponse: LocalizedError {
    var errorDescription: String?{
        return statusMessage
    }
}
