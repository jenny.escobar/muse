//
//  ViewController.swift
//  muse
//
//  Created by Jenny escobar on 12/10/21.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var synonymsTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var synonyms = [Synonym]()
    var selectedIndex = 0
    var currentSearchTask: URLSessionTask?
    var datamuseClient: DatamuseClient!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datamuseClient = DatamuseClient()
        self.searchBar.delegate = self
        synonymsTableView.delegate = self
        synonymsTableView.dataSource = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "synonymDetail" {
            let detailVC = segue.destination as! SynonymsDetailsViewController
            detailVC.synonym = synonyms[selectedIndex]
        }
    }

}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        currentSearchTask?.cancel()
        currentSearchTask = datamuseClient.getMuseByMeaning(word: searchText) { synonyms, error in
            self.synonyms = synonyms
            DispatchQueue.main.async {
                self.synonymsTableView.reloadData()
            }
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return synonyms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SynonymCell")!
        
        let synonym = synonyms[indexPath.row]
        
        cell.textLabel?.text = "\(synonym.word)"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "synonymDetail", sender: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
