//
//  VocabularyViewController.swift
//  muse
//
//  Created by Jenny escobar on 15/10/21.
//

import UIKit
import CoreData


class VocabularyViewController: UIViewController {
    
    @IBOutlet weak var vocabularyTableView: UITableView!
    var vocabulary: [NSManagedObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        vocabularyTableView.delegate = self
        vocabularyTableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)

      guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
      }

      let managedContext = appDelegate.persistentContainer.viewContext
      let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Vocabulary")

      do {
        vocabulary = try managedContext.fetch(fetchRequest)
      } catch let error as NSError {
        print("Could not fetch. \(error), \(error.userInfo)")
      }
    }

}

extension  VocabularyViewController: UITableViewDataSource, UITableViewDelegate  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return vocabulary.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      let person = vocabulary[indexPath.row]
      let cell = tableView.dequeueReusableCell(withIdentifier: "VocabularyCell", for: indexPath)
      cell.textLabel?.text = person.value(forKeyPath: "word") as? String
      return cell
    }
}
