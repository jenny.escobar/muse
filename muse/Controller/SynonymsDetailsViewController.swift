//
//  SynonymsDetailsViewController.swift
//  muse
//
//  Created by Jenny escobar on 14/10/21.
//

import UIKit
import CoreData

class SynonymsDetailsViewController: UIViewController {
    
    var synonym: Synonym!
    var tags:[String]!

    @IBOutlet weak var detailsTitle: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var tagsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailsTitle.text = synonym.word
        self.score.text = String(synonym.score)
        self.tagsTableView.delegate = self
        self.tagsTableView.dataSource = self
        tags = synonym.tags ?? []
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        print(synonym.word)
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }

        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Vocabulary", in: managedContext)!
        let vocabulary = NSManagedObject(entity: entity, insertInto: managedContext)
        vocabulary.setValue(synonym.word, forKeyPath: "word")

        do {
          try managedContext.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        dismiss(animated: true, completion: nil)
        //performSegue(withIdentifier: "showVocabulary", sender: nil)
    }
    
}


extension SynonymsDetailsViewController: UITableViewDataSource, UITableViewDelegate  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TagCell")!
        let tag = tags[indexPath.row]
        cell.textLabel?.text = tag
        return cell
    }
    
}
